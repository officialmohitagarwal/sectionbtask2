import './App.css';
import { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';

function App() {
  const [posts, setPosts] = useState([]);
  const [index, setIndex] = useState(0);
  const [originalList, setOriginalList] = useState([]);

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((res) => res.json())
      .then((postsRes) => {
        setTimeout(() => {
          setOriginalList(postsRes);
          setIndex(10);
          setPosts(postsRes.filter((el, i) => i < 10));
        }, 3000);
      });
  }, []);

  const fetchMoreData = () => {
    setTimeout(() => {
      setPosts([
        ...originalList.filter((el, i) => i > index && i <= index + 10),
        ...posts,
      ]);
      setIndex(index + 10);
    }, 3000);
  };

  return (
    <main>
      <h1 style={{ marginLeft: '2rem', marginTop: '1rem' }}>
        Lazy Loading Skeleton
      </h1>
      <div>
        {posts.length > 0 ? (
          <InfiniteScroll
            className='grid'
            dataLength={posts.length}
            next={fetchMoreData}
            hasMore={index < 90}
            loader={
              <>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
                <div className='card'>
                  <div className='flex'>
                    <img
                      className='header-img skeleton'
                      src='https://source.unsplash.com/100x100/?nature'
                      alt=''
                    />
                    <div className='heading' data-title>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                      <div className='skeleton skeleton-text-lg skeleton-text'></div>
                    </div>
                  </div>
                  <div data-body>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                    <div className='skeleton skeleton-text'></div>
                  </div>
                </div>
              </>
            }
            endMessage={<p>You've reached the end</p>}
          >
            {posts.map((el) => (
              <div className='card'>
                <div className='flex'>
                  <img
                    className='header-img skeleton'
                    src='https://source.unsplash.com/100x100/?nature'
                    alt=''
                  />
                  <div className='heading' data-title>
                    {el.title}
                  </div>
                </div>
                <div data-body>{el.body}</div>
              </div>
            ))}
          </InfiniteScroll>
        ) : (
          <div className='grid'>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
            <div className='card'>
              <div className='flex'>
                <img
                  className='header-img skeleton'
                  src='https://source.unsplash.com/100x100/?nature'
                  alt=''
                />
                <div className='heading' data-title>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                  <div className='skeleton skeleton-text-lg skeleton-text'></div>
                </div>
              </div>
              <div data-body>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
                <div className='skeleton skeleton-text'></div>
              </div>
            </div>
          </div>
        )}
      </div>
    </main>
  );
}

export default App;
